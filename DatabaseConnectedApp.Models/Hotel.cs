﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnectedApp.Models
{
    public class Hotel
    {
        public int Id { get; set; }
        public string HotelName { get; set; }
        public string HotelCity { get; set; }
        public int HotelStar { get; set; }
        public int HotelPriceForDay { get; set; }
        public int HotelRooms { get; set; }
        public int HotelFreeRooms { get; set; }

    }
}
