﻿using DatabaseConnectedApp.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnectedApp.DataAccess
{
    public class HotelTableDataService
    {
        private readonly string _connectionString;
        public HotelTableDataService()
        {
            _connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Султановак\source\repos\MyApp\DatabaseConnectedApp.DataAccess\Hotels.mdf;Integrated Security=True";
        }
        public List<Hotel> GetAllHotels()
        {
            var data = new List<Hotel>();

            using (var connection = new SqlConnection(_connectionString))
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.Open();
                    command.CommandText = "select * from HotelsTable";

                    var sqlDataReader = command.ExecuteReader();
                    while(sqlDataReader.Read())
                    {
                        int id = (int)sqlDataReader["Id"];
                        string hotelName = sqlDataReader["HotelName"].ToString();
                        string hotelCity = sqlDataReader["HotelCity"].ToString();
                        int hotelStar = (int)sqlDataReader["HotelStar"];
                        int hotelPriceForDay = (int)sqlDataReader["HotelPriceForDay"];
                        int hotelRooms = (int)sqlDataReader["HotelRooms"];
                        int hotelFreeRooms = (int)sqlDataReader["HotelFreeRooms"];

                        data.Add(new Hotel
                        {
                            Id = id,
                            HotelName = hotelName,
                            HotelCity = hotelCity,
                            HotelStar = hotelStar,
                            HotelPriceForDay = hotelPriceForDay,
                            HotelRooms = hotelRooms,
                            HotelFreeRooms = hotelFreeRooms
                        });
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                return data;
            }
        }
    }
}
