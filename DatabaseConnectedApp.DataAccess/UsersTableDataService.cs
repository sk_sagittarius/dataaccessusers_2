﻿using DatabaseConnectedApp.Models;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Configuration;

namespace DatabaseConnectedApp.DataAccess
{
    public class UsersTableDataService
    {
        private readonly string _connectionString; // readonly - только что чтения, задается на этапе создания кода
        private readonly string _providerName; 
        private readonly DbProviderFactory _providerFactory; 

        public UsersTableDataService() // задание _connectionString через конструктор
        {

            //_connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Султановак\source\repos\DatabaseConnectedApp\DatabaseConnectedApp.DataAccess\Database.mdf;Integrated Security=True";
            _connectionString = ConfigurationManager.ConnectionStrings["testConnectionString"].ConnectionString; // достаем строку из объекта ConnectionStrings

            _providerName = ConfigurationManager.ConnectionStrings["testConnectionString"].ProviderName; // получение имени провайдера

            _providerFactory = DbProviderFactories.GetFactory(_providerName); // получение фабрики провайдера
        }
        public List<User> GetAllUsers()
        {
            string isAdminValue = ConfigurationManager.AppSettings["isAdmin"];
            if(bool.TryParse(isAdminValue, out bool isAdmin) && isAdmin) // если удалось распарсить и если isAdmin = true, тогда
            {
                Console.WriteLine("***Admin Mode***");
            }

            var data = new List<User>(); // буферный список юзеров

            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();
                    command.CommandText = "select * from Users";

                    var dataReader = command.ExecuteReader();

                    while(dataReader.Read())
                    {
                        int id = (int)dataReader["Id"];
                        string login = dataReader["Login"].ToString();
                        string password = dataReader["Password"].ToString();

                        data.Add(new User // добавление в буфер нового пользователя
                        {
                            Id = id,
                            Login = login,
                            Password = password
                        });
                    }
                    dataReader.Close();
                }
                catch (DbException exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    //TODO обработка ошибки
                    throw;
                }
            }
            return data;
        }

        public void AddUser(User user)
        {
            using (var connection = _providerFactory.CreateConnection())
            using (var command = connection.CreateCommand())
            {
                DbTransaction transaction = null; // создаем и обнуляем переменную для транзакции
                try
                {
                    connection.ConnectionString = _connectionString;
                    connection.Open();

                    transaction = connection.BeginTransaction();

                    command.CommandText = $"insert into Users values(@login, @password)"; // безопасный запрос с параметрами
                    command.Transaction = transaction; // сообщаем комманде что она транзакция. указываем для каждой комманды

                    //SqlParameter loginParameter = new SqlParameter
                    //{
                    //    ParameterName = "@login",
                    //    SqlValue = user.Login, //определяется тип объекта
                    //    SqlDbType = SqlDbType.NVarChar,
                    //    IsNullable = false
                    //};

                    DbParameter loginParameter = command.CreateParameter();
                    loginParameter.ParameterName = "@login";
                    loginParameter.Value = user.Login; //определяется тип объекта
                    loginParameter.DbType = DbType.String;
                    loginParameter.IsNullable = false;
                    
                    //SqlParameter passwordParameter = new SqlParameter
                    //{
                    //    ParameterName = "@password",
                    //    SqlValue = user.Login,
                    //    SqlDbType = SqlDbType.NVarChar,
                    //    IsNullable = false
                    //};

                    DbParameter passwordParameter = command.CreateParameter();
                    passwordParameter.ParameterName = "@password";
                    passwordParameter.Value = user.Login;
                    passwordParameter.DbType = DbType.String;
                    passwordParameter.IsNullable = false;
                    

                    command.Parameters.AddRange(new DbParameter[] { loginParameter, passwordParameter }); // добавление (передача) параметров


                    var affectedRows = command.ExecuteNonQuery();

                    if(affectedRows<1)
                    {
                        throw new Exception("Вставка не была произведена");
                    }

                    transaction.Commit(); // транзакция должна быть зафиксирована
                    transaction.Dispose();
                }
                catch (DbException exception)
                {
                    transaction?.Rollback(); // откат транзакции
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
                catch (Exception exception)
                {
                    transaction?.Rollback(); // откат транзакции
                    transaction.Dispose();
                    //TODO обработка ошибки
                    throw;
                }
            }
        }

        public void DeleteUserById(int id)
        {

        }

        public void UpdateUser(User user)
        {

        }
    }
}
