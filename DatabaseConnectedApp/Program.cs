﻿using DatabaseConnectedApp.DataAccess;
using DatabaseConnectedApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnectedApp.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataService = new UsersTableDataService();
            dataService.AddUser(new User
            {
                Login = "admin",
                Password = "root"
            });
            foreach (var user in dataService.GetAllUsers())
            {
                System.Console.WriteLine($"{user.Id}, {user.Login}, {user.Password}");
            }
            //System.Console.ReadLine();

            var dataHotelService = new HotelTableDataService();
            foreach (var hotel in dataHotelService.GetAllHotels())
            {
                System.Console.WriteLine($"{hotel.Id}, {hotel.HotelName}, {hotel.HotelCity}, {hotel.HotelStar}, {hotel.HotelPriceForDay}, {hotel.HotelRooms}, {hotel.HotelFreeRooms}");
            }
            System.Console.ReadLine();

           
        }
    }
}
